require 'httparty'
require 'yaml'

def gather_events(project_id, start_date, end_date)
  events_api_url_base = "https://gitlab.com/api/v4/projects/#{project_id}/events?after=#{start_date - 1}&before=#{end_date + 1}&per_page=100"

  puts "Calling API for Events..."
  puts events_api_url_base

  response = HTTParty.get(events_api_url_base)
  events = response.parsed_response

  while(response.headers["x-next-page"] != "")
    puts "Found #{events.count} events so far..."

    events_api_url = events_api_url_base + "&page=#{response.headers["x-next-page"]}"

    puts "Calling API for more Events..."
    puts events_api_url

    response = HTTParty.get(events_api_url)

    events << response.parsed_response
    events.flatten!
  end

  puts "Found a total of #{events.count} events"

  events
end

def unique_event_authors(events)
  events_authors = events.map do |event|
    { 
      id: event["author_id"],
      username: event["author_username"],
      name: event["author"]["name"]
    }
  end

  uniq_event_authors = events_authors.uniq

  puts "Found #{uniq_event_authors.count} unique authors for a total of #{events.count} events"

  uniq_event_authors
end

def gather_members(member_group_id, token)
  group_members_api_url_base = "https://gitlab.com/api/v4/groups/#{member_group_id}/members?private_token=#{token}&per_page=100"

  puts "Calling API for Group members..."
  puts group_members_api_url_base

  response = HTTParty.get(group_members_api_url_base)

  members = response.parsed_response.map do |h| 
    { 
      id: h["id"],
      username: h["username"],
      name: h["name"]
    }
  end

  while(response.headers["x-next-page"] != "")
    puts "Found #{members.count} members so far..."

    group_members_api_url = group_members_api_url_base + "&page=#{response.headers["x-next-page"]}"

    puts "Calling API for more group members..."
    puts group_members_api_url

    response = HTTParty.get(group_members_api_url)

    response.parsed_response.each do |h| 
      members << { 
        id: h["id"],
        username: h["username"],
        name: h["name"]
      }
    end
  end

  puts "Found #{members.count} members for Group"

  members
end

def reduce_event_authors(authors, members)
  authors - members
end

def pick_prizewinners(authors, prizes)
  total_prizes = prizes.inject(0) { |i,k| i+=k[1] }

  winners = authors.sample(total_prizes)

  prizes.map do |prize|
    prize_arr = []
    prize_name = prize[0]
    num_per_prize = prize[1]
    num_per_prize.times do
      prize_arr << winners.pop
    end
    
    {
      name: prize_name,
      winners: prize_arr
    }
  end
end

def export_winners(event_name, prizes, filename)
  winner_text = ["Prizewinners for #{event_name}"]
  winner_text << "Total number of prizes - #{prizes.count}"
  winner_text << "Total number of prizewinners - #{prizes.map { |p| p[:winners] }.flatten.uniq.count}"

  prizes.each do |prize|
    winner_text << "Winners of #{prize[:name]}"
    prize[:winners].each do |winner|
      winner_text << "#{winner[:username]} - #{winner[:name]}"
    end
  end

  winner_text.each { |t| puts t }

  File.open(filename, "wb") do |io|
    winner_text.each { |t| io.puts t }
  end; nil
end

def export_events(events, filename)
  csv_data = csv_start_data

  events.each do |event|
    csv_data << format_event_data(event)
  end

  CSV.open(filename, "wb") do |csv|
    csv_data.each { |row| csv << row }
  end; nil
end

def csv_start_data
  [csv_headers]
end

def csv_headers
  [
    "action","target_type","target_title","created_at",
    "author_name","author_username","note_body", "noteable_type"
  ]
end

def format_event_data(event)
  [
    event["action_name"].to_s,
    event["target_type"].to_s,
    event["target_title"],
    DateTime.parse(event["created_at"]).to_date.to_s,
    event["author"] ? event["author"]["name"] : nil,
    event["author"] ? event["author"]["username"] : nil,
    event["note"] ? event["note"]["body"] : nil,
    event["note"] ? event["note"]["noteable_type"] : nil
  ]
end

event_config = YAML.load_file('event_config.yml')
events = gather_events(event_config['project_id'], event_config['start_date'], event_config['end_date'])
authors = unique_event_authors(events)
members = gather_members(event_config['member_group_id'], event_config['token'])
eligible_authors = reduce_event_authors(authors, members)

prizes = pick_prizewinners(eligible_authors, event_config['prizes'])
export_winners(event_config['name'], prizes, "winners.txt")
export_events(events, "events.csv")

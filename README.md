### GitLab Issue Bash Prize Winner Calculator

We plan to publish how Issue Bash prize winners are chosen here.

All suggestions for how to make the prize winning process as fair as possible are welcomed. Please raise an issue.

We expect the calculation to evolve for a while